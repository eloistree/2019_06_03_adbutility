﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class UI_ADBUtility_ExtractPackage : MonoBehaviour
{
    public AdbUtilityMono m_utility;
    public AdbCmdExtractApkParameter m_parameters;
    public InputField m_apkPhoneFilePath;
    public InputField m_whereToInstall;
    public InputField m_applicationApkName;
    public Button m_installApk;


    public void Apply()
    {
        m_utility.ExtractPackage(m_parameters);
    }

    public void Update()
    {
        if (string.IsNullOrEmpty(m_whereToInstall.text)) {
            m_whereToInstall.text = AdbUtillity.m_apkFolderPath;
        }
        m_installApk.interactable = File.Exists(m_parameters.GetWindowPath());

        m_parameters.SetApkPathOnPhone(m_apkPhoneFilePath.text);
        m_parameters.SetDestinationFolder(m_whereToInstall.text);
        m_parameters.SetApplicationName(m_applicationApkName.text);

    }

    public void Install() {
        if (File.Exists(m_parameters.GetWindowPath())) {
            m_utility.Install(m_parameters.GetWindowPath());
        }
    }

}