﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ADBUtility_Connect : MonoBehaviour
{
    public AdbUtilityMono m_utility;

    public InputField m_ipAddress;

    public void Apply()
    {
        
        m_utility.ConnectToIpAddress(m_ipAddress.text);
    }

}