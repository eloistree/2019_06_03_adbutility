﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;

public class AdbUtillity 
{

    public static string m_adbFolderPath;
    public static string m_scrcopyFolderPath;
    public static string m_apkFolderPath;
    public static string m_screenshotFolderPath;
    public static string m_appDataFolderPath;

    internal static void ExtractPackage(AdbCmdExtractApkParameter extractPackage)
    {
        throw new NotImplementedException();
    }

    public static Queue<string> m_output = new Queue<string>();
    public static Queue<string> m_outputError = new Queue<string>();

    public static string GetAPKFolder()
    {
        if (string.IsNullOrEmpty(m_apkFolderPath))
            return m_adbFolderPath;
        return m_apkFolderPath;
    }

    public static int m_maxOutputRecord = 5000;

    public static Dictionary<string, AndroidDevice> m_registeredDevices = new Dictionary<string, AndroidDevice>();

   
    public static void AddRegisteredDevices( AndroidDevice info)
    {
        if (!m_registeredDevices.ContainsKey(info.m_deviceId))
            m_registeredDevices.Add(info.m_deviceId, info);
    }
    
    public static void SetRegisteredDevices( AndroidDevice info)
    {
        AddRegisteredDevices(info);
        m_registeredDevices[info.m_deviceId] = info;
    }

    internal static void ClearRegisteredDevices()
    {
        m_registeredDevices.Clear();
    }

    public static AndroidDevice GetDeviceRegistered(string id) {
        if(m_registeredDevices.ContainsKey(id))
            return m_registeredDevices[id];
        return null;
    }

    
    internal static List<AndroidDevice> GetRegisteredDevice()
    {
        return m_registeredDevices.Values.ToList();
    }
   

    internal static void GetDeviceList(out RawOuputCallBack callback)
    {
        ApplyCommands(m_adbFolderPath,0, out callback, "adb devices -l");
    }
    internal static void GetApplicationInstallation(out RawOuputCallBack callback, string deviceId, int maxWaitingTimeMiliSeconds = 800)
    {
        ApplyCommands(m_adbFolderPath, maxWaitingTimeMiliSeconds, out callback, "adb shell pm list packages -f -i -3", deviceId);
    }

    internal static void KillAdbServer()
    {
        RawOuputCallBack callback;
        ApplyCommands(m_adbFolderPath, 0, out callback, "adb kill-server");
    }

    public static void StartTcpIpOnDevice(int port, string [] devices)
    {
        string cmd = "adb tcpip " + port;
        ApplyCommands(m_adbFolderPath, 0, cmd, devices);
    }
    public static void ConnectToIpAddress(string text)
    {
        RawOuputCallBack callback;
        ApplyCommands(m_adbFolderPath, 0, out callback, "adb connect "+text);
    }


    internal static void LaunchBluetoothSetting( string deviceId) {

        RawOuputCallBack callback;
        ApplyCommands(m_adbFolderPath, 0, out callback, "adb shell am start -a android.settings.BLUETOOTH_SETTINGS", deviceId);
    }

    internal static void DisplayIpAddress(string[] m_devicesId)
    {
        ApplyCommands(m_adbFolderPath, 200, "adb shell ip addr show wlan0", m_devicesId);
        
    }

    internal static void ApplyAdbCmdParameter(AdbCmdParameterAbstract parameter, params string [] devicesId)
    {
        UnityEngine.Debug.Log("eee");
        for (int i = 0; i < devicesId.Length; i++)
        {
            RawOuputCallBack callback;
            ApplyAdbCmdParameter(parameter, out callback, devicesId[i]);

        }
    }
        internal static void ApplyAdbCmdParameter(AdbCmdParameterAbstract parameter, out RawOuputCallBack callback, string deviceId)
    {
        parameter.SetTargetWith( deviceId);

        string cmd = parameter.GetTheCommand();
        UnityEngine.Debug.Log("Cmd: " + cmd);
        if (parameter is AdbCmdScreencastParameter)
        {
            ApplyCommandsWithoutReturn(m_scrcopyFolderPath, cmd);
            callback = RawOuputCallBack.CreateEmptyDefault();
        }
        if (parameter is AdbCmdLaunchApkParameter)
        {
            ApplyCommandsWithoutReturn(m_adbFolderPath, cmd);
            callback = RawOuputCallBack.CreateEmptyDefault();
        }
        else ApplyCommands(m_adbFolderPath, 0, out callback, cmd);

    }


    public static void RestartTheDevice(string deviceId)
    {
        RawOuputCallBack callback;
        ApplyCommands(m_adbFolderPath, 0, out callback, "adb reboot", deviceId);
    }
    public static void TurnDeviceOff(string deviceId)
    {
        RawOuputCallBack callback;
        ApplyCommands(m_adbFolderPath, 0, out callback, "adb shell reboot -p", deviceId);
    }

    

    #region To code
    public void GetPathOfApplicationOnDevice()
    {

        //adb shell pm list packages List of application install
    }



    //adb devices List of connected devices
    //adb shell pm path com.example.someapp   Get Path of the application on the device
    //adb pull /data/app/com.example.someapp.apk path/to/desired/destination Extract apk to the path location
    //adb install pathofthe.apk Install the application at the following path
    //adb shell am start -a android.settings.BLUETOOTH_SETTINGS Launch Bluetooth setting
    //adb tcpip 5555	Set the port of the tcp ip
    //adb connect 192.168.1.113:5555	Set the device as contactable at this ip
    //adb kill-server reset your adb host
    //scrcpy -c 1360:680:10:240	Steam the screen with a crop on the
    //scrcpy -f Just stream the screen
    //adb install btsettingslauncher.apk Install the Bluetooth app to set setting from the device.
    //adb uninstall btsettingslauncher.apk Uninstall the Bluetooth app to set setting from the device.
    //adb push local remote   Send file to the Android (ex: /sdcard/foo.txt)
    //adb pull remote local   Received file fro the android(ex: /sdcard/foo.txt)
    //adb shell ls List of files
    #endregion



    internal static void StartScreenCastingWithThread(AdbCmdScreencastParameter parameter, string  [] devicesid)
    {
        for (int i = 0; i < devicesid.Length; i++)
        {
            parameter.SetTargetWith(devicesid[i]);
            StartScreenCastingWithThread(parameter, false);
        }
    }
    
    private static AdbCmdScreencastParameter nextCastingParameters;
    private static List <Thread> screenRecordThread = new List<Thread>();

    internal static void StartScreenCastingWithThread(AdbCmdScreencastParameter parameter, string devicesid, bool killOthers = true)
    {
            parameter.SetTargetWith(devicesid);
            StartScreenCastingWithThread(parameter, killOthers);
    }

    internal static void OpenNotePad(string text)
    {
        string path = Application.persistentDataPath.Trim()+"/nodepadlog.txt";
        UnityEngine.Debug.Log("DDS: " + path);
        File.WriteAllText(path, text);
        string cmd = "start notepad \""+ path + "\"";
        ApplyCommandsWithoutReturn(cmd);
    }

    internal static void StartScreenCastingWithThread(AdbCmdScreencastParameter parameters, bool killOthers=true)
    {
        //if (killOthers && screenRecordThread != null)
        //{
        //    KillAllAdbBroadcasting();
        //}
        nextCastingParameters = (parameters);
        Thread t = new Thread(StartDefaultScreenCastingThread);
        screenRecordThread.Add(t); 
        t.Start();
    }

    private static void KillAllAdbBroadcasting()
    {
        for (int i = 0; i < screenRecordThread.Count; i++)
        {
            screenRecordThread[i].Abort();
        }

        KillProcessByName("ADB");
    }

    private static void StartDefaultScreenCastingThread() {

        ApplyCommandsWithoutReturn(m_scrcopyFolderPath,
            (nextCastingParameters) ==null?
            "scrcpy -f":
            nextCastingParameters.GetTheCommand() );
      
    }

    private static void KillProcessByName(string name)
    {
        try
        {
            foreach (Process proc in Process.GetProcessesByName(name))
            {
                proc.Kill();
            }
        }
        catch (Exception ex)
        {
        }
    }

    public static string SetCommandForThisTarget(string target, string command, string appName = "adb")
    {
        if (string.IsNullOrEmpty(target))
            return command;
        int adbIndex = command.ToLower().IndexOf(appName + " ");
        if (adbIndex < 0)
            return command;

        if (command.ToLower().IndexOf(" -s ") < 0)
        {

            return command.Insert(adbIndex + (appName.Length + 1), " -s " + target + " ");
        }
        return command;
    }

    internal static void SetScreenRecorderFolderPath(string scrcopyPath)
    {
        m_scrcopyFolderPath = scrcopyPath;
    }

    public static bool m_hideWindow=true;
    public static Process process;

    internal static void DisplayInConsoleProcessorsName() {
        foreach (Process pr in Process.GetProcesses())
        {
            try
            {
                UnityEngine.Debug.Log(string.Format("App Name: {0}, Process Name: {1}", Path.GetFileName(pr.MainModule.FileName), pr.ProcessName));
            }
            catch { }
        }
    }
    internal static void ApplyCommandsWithoutReturn(string command)
    {
        ApplyCommandsWithoutReturn("", command);
    }

        internal static void ApplyCommandsWithoutReturn(string executableFolderPath, params string [] commands)
    {


       

        //if (process != null)
        //{   
        //    process.Kill();
        //}
        process = new Process();
        {
            
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.WorkingDirectory = @"C:\";
            process.StartInfo.FileName = Path.Combine(Environment.SystemDirectory, "cmd.exe");
            //if (m_hideWindow)
            //    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            // Redirects the standard input so that commands can be sent to the shell.
            process.StartInfo.RedirectStandardInput = true;
            // Runs the specified command and exits the shell immediately.
            //process.StartInfo.Arguments = @"/c ""dir""";

            process.OutputDataReceived += ProcessOutputDataHandler;
            process.ErrorDataReceived += ProcessErrorDataHandler;

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            string disk = "C";
            string rootPath = "";

            if (!string.IsNullOrEmpty(executableFolderPath)) {

                disk = executableFolderPath.Substring(0, 1);

                process.StandardInput.WriteLine(disk.ToString() + ":");
                process.StandardInput.Flush();

               // UnityEngine.Debug.Log("DD> cd " + executableFolderPath);
                process.StandardInput.WriteLine("cd " + executableFolderPath);
                process.StandardInput.Flush();
                rootPath = executableFolderPath + "\\";
            }
            
            for (int i = 0; i < commands.Length; i++)
            {


                process.StandardInput.WriteLine(rootPath + commands[i]);
                process.StandardInput.Flush();
                UnityEngine.Debug.Log("DD> " + rootPath + commands[i]);
               // UnityEngine.Debug.Log(executableFolderPath + "\\" + commands[i]);
            }
            process.WaitForExit();
            process.Close();

        }

    }




    // C:/adb/adb
    // echo "hey"

    internal static void ApplyCommands(string executableFolderPath, int milliscondToWait, string command, string [] devicesID)
    {
        for (int i = 0; i < devicesID.Length; i++)
        {
            RawOuputCallBack cb;
            ApplyCommands(executableFolderPath, milliscondToWait, out cb, command, devicesID[i]);
        }

    }
    internal static void ApplyCommands(string executableFolderPath, int milliscondToWait, out RawOuputCallBack callback, string command, string devicesID)
    {
       
            string newCmd = SetCommandForThisTarget(devicesID, command);
            ApplyCommands(executableFolderPath, milliscondToWait, out callback, newCmd);

    }
        internal static void ApplyCommands(string executableFolderPath, int milliscondToWait , out RawOuputCallBack callback,  string command)
    {

        m_output.Enqueue("> Send: "+command);
        string cmd = executableFolderPath + "\\" + command;
        callback = new RawOuputCallBack();
        callback.m_command = command;
        if (string.IsNullOrEmpty(executableFolderPath) || command.Length <= 0) {
            callback.NotifyAsFinish();
            return;
            
        }

        //Source: https://stackoverflow.com/questions/11767654/sending-commands-to-cmd-prompt-in-c-sharp/11768260#11768260
        using (Process process = new Process())
        {
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.WorkingDirectory = @"C:\";
            process.StartInfo.FileName = Path.Combine(Environment.SystemDirectory, "cmd.exe");

            if (m_hideWindow)
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            // Redirects the standard input so that commands can be sent to the shell.
            process.StartInfo.RedirectStandardInput = true;
            // Runs the specified command and exits the shell immediately.
            //process.StartInfo.Arguments = @"/c ""dir""";

            process.OutputDataReceived += ProcessOutputDataHandler;
            process.ErrorDataReceived += ProcessErrorDataHandler;

            List<string> values = new List<string>();
            process.OutputDataReceived += delegate (object sendingProcess, DataReceivedEventArgs outLine) {
                values.Add(outLine.Data);
            };

            process.Start();
            Thread.Sleep(milliscondToWait);
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            process.StandardInput.WriteLine(cmd);

            process.StandardInput.Flush();
            // Send a directory command and an exit command to the shell
            //for (int i = 0; i < commands.Length; i++)
            //{

            //    process.StandardInput.WriteLine(executableFolderPath + "\\" + commands[i]);
            //    process.StandardInput.Flush();
            //}

            process.StandardInput.WriteLine("exit");
            if (milliscondToWait == 0)
                process.WaitForExit();
            else {

                Thread.Sleep(milliscondToWait);
                if(process!=null && !process.HasExited)
                process.Kill();
            }

            process.Close();
            callback.AddOutput(values);
            callback.NotifyAsFinish();

        }
    }

    public static void SetAdbFolderPath(string adbFolder)
    {
        m_adbFolderPath = adbFolder;
    }

    public static void ProcessOutputDataHandler(object sendingProcess, DataReceivedEventArgs outLine)
    {
        string data = outLine.Data;
        if (string.IsNullOrEmpty(data))
            return;
        if (data.IndexOf(">exit") >= 0)
            return;
        if (data.IndexOf("Microsoft Windows [") >= 0)
            return;
        if (data.IndexOf("(c) 2018 Microsoft Corporation") >= 0)
            return;
        if (data.IndexOf(m_adbFolderPath) >= 0)
            return;
        if (data.IndexOf("C:\\>echo") >= 0)
            return;
        if (data.IndexOf("C:\\> C:") >= 0)
            return;

        UnityEngine.Debug.Log("Hey: " + outLine.Data);
        m_output.Enqueue(data);
    }

    public static void ProcessErrorDataHandler(object sendingProcess, DataReceivedEventArgs outLine)
    {
        m_outputError.Enqueue(outLine.Data);
    }

    public static void OpenWindowConsole() {
        Process p = new Process();
        p.StartInfo.UseShellExecute = true;
        p.StartInfo.WorkingDirectory = @"C:\";
        p.StartInfo.FileName = Path.Combine(Environment.SystemDirectory, "cmd.exe");
        p.Start();
    }

}

[System.Serializable]
public class RawOuputCallBack
{
    public string m_command = "";
    public OuputGroups m_outputs = new OuputGroups();
    [SerializeField] bool m_finished = false;
    public ConsoleOutputsDelegate m_outputsFound;
    public void NotifyAsFinish()
    {
        m_finished = true;
        if(m_outputsFound!=null)
            m_outputsFound(m_outputs.GetValuesAsArray());
    }
    public bool IsFinished()
    {
        return m_finished;
    }

    public void AddOutput(string output)
    {
        m_outputs.AddOutput(output);
    }
    public void AddOutput(IEnumerable<string> output)
    {
        foreach (var item in output)
        {
            m_outputs.AddOutput(item);

        }
    }

    internal static RawOuputCallBack CreateEmptyDefault()
    {
        RawOuputCallBack cb = new RawOuputCallBack();
        cb.NotifyAsFinish();
        return cb;
    }
}
[System.Serializable]
public class OuputGroups
{
    public List<string> m_outputs = new List<string>();

    internal string[] GetValuesAsArray()
    {
        return m_outputs.ToArray();
    }
    public void AddOutput(string output)
    {
        m_outputs.Add(output);

    }
}

[System.Serializable]
public abstract class AdbCmdParameterAbstract {

    public string GetTheCommand() {
        return GetTheCommand(m_specificTarget);
    }

    public abstract string GetTheCommand(string targetId);


    [SerializeField] string m_specificTarget;
    public void SetTargetWith(string id)
    {
        m_specificTarget = id;
    }
}
[System.Serializable]
public class AdbCmdScreencastParameter : AdbCmdParameterAbstract {

    public enum CropType { QuestFullScreen, QuestLeftEye, QuestLeftRight }


    public override string GetTheCommand(string targetDevicesID) {

        string cmd = "scrcpy";

        if (!string.IsNullOrWhiteSpace(targetDevicesID))
            cmd += " -s " + targetDevicesID;
        

        if (m_useMaxSize)
            cmd += string.Format(m_maxSizeFormat, m_maxSize);
        if (m_useBitRate)
            cmd += string.Format(m_bitRateFormat, m_bitRate);
        if (m_useCrop)
            cmd += string.Format(m_cropFormat, m_width, m_height, m_offsetX, m_OffsetY);
        if (m_usingRecorder)
            cmd += string.Format(m_recordFormat, m_recorderPath);
        if (m_useFullscreen)
            cmd += m_fullscreenFormat;
        if (m_useAlwaysOnTop)
            cmd += m_alwaysOnTopFormat;
        if (m_useTouches)
            cmd += m_touhcesFormat;
        if (m_useNoDisplay)
            cmd += m_noDisplayFormat;

        return cmd;
    }

    public bool m_useMaxSize;
    public float m_maxSize = 1024;
    private string m_maxSizeFormat = " --max-size {0}";

    public bool m_useBitRate;
    public int m_bitRate = 2;
    private string m_bitRateFormat = " --bit-rate {0}M";

    public bool m_useCrop;
    public int m_width = 28880;
    public int m_height = 1600;
    public int m_offsetX = 0;
    public int m_OffsetY = 0;
    private string m_cropFormat = " --crop {0}:{1}:{2}:{3}";

    public bool m_usingRecorder;
    public string m_recorderPath = "/sdcard/record/lastrecord.mp4";
    private string m_recordFormat = " --record {0}";

    public bool m_useFullscreen = true;
    private string m_fullscreenFormat = " --fullscreen";

    public bool m_useNoDisplay = true;
    private string m_noDisplayFormat = " --no-display";

    public bool m_useAlwaysOnTop;
    private string m_alwaysOnTopFormat = " --always-on-top";

    public bool m_useTouches;
    private string m_touhcesFormat = " --show-touches";


    public void SetCropWithQuestSize()
    {
        m_width = 2880;
        m_height = 1600;
        m_offsetX = 0;
        m_OffsetY = 0;
    }

    public void SetCropWithLeftEyes()
    {
        m_width = 2880 / 2;
        m_height = 1600;
        m_offsetX = 0;
        m_OffsetY = 0;
    }
    public void SetCropWithRightEyes()
    {
        m_width = 2880 / 2;
        m_height = 1600;
        m_offsetX = 2880 / 2;
        m_OffsetY = 0;
    }

    internal static AdbCmdScreencastParameter CreateDefault()
    {
        return new AdbCmdScreencastParameter() { m_useFullscreen = true };
    }

    public void SetRecordingPathTo(string text)
    {
        m_recorderPath = StringUtility.FinishBy(text, ".mp4");
    }

    public void SetFullscreenTo(bool isOn)
    {
        m_useFullscreen = isOn;
    }

    public void SetNotDisplayTo(bool isOn)
    {
        m_useNoDisplay = isOn;
    }

    public void SetTouchesTo(bool isOn)
    {
        m_useTouches = isOn;
    }

    public void SetCropTo(bool isOn)
    {
        m_useCrop = isOn;
    }

    public void SetCropValueTo(string width, string height, string x, string y)
    {
        try {
            m_width = int.Parse(width);
            m_height = int.Parse(height);
            m_offsetX = int.Parse(x);
            m_OffsetY = int.Parse(y);
        }
        catch (Exception e) { }

    }
    public void SetCropValueTo(int width, int height, int x, int y)
    {

        m_width = width;
        m_height = height;
        m_offsetX = x;
        m_OffsetY = y;

    }

    public void SetBitRateValueTo(string text)
    {
        text = text.ToLower();
        text = text.Replace("m", "");
        try
        {
            m_bitRate = int.Parse(text);
        }
        catch (Exception) {
            m_bitRate = 2;
        }
    }

    public void SetMaxSizeValueTo(string text)
    {
        try
        {
            m_maxSize = int.Parse(text);
        }
        catch (Exception)
        {
            m_maxSize = 1024;
        }
    }

    public void SetOnTopTo(bool isOn)
    {
        m_useAlwaysOnTop = isOn;
    }

    public void SetRecordingTo(bool isOn)
    {
        m_usingRecorder = isOn;
    }

    public void SetBitRateTo(bool isOn)
    {
        m_useBitRate = isOn;
    }

    public void SetMaxSizeTo(bool isOn)
    {
        m_useMaxSize = isOn;
    }

    public static void GetValueFor(CropType type, out int width, out int height, out int x, out int y)

    {
        width = height = x = y = 0;

        switch (type)
        {
            case CropType.QuestFullScreen:

                width = 2860;
                height = 1430;
                x = 0;
                y = 0;
                break;
            case CropType.QuestLeftEye:
                width = 1430;
                height = 1430;
                x = 0;
                y = 0;
                break;
            case CropType.QuestLeftRight:
                width = 1430;
                height = 1430;
                x = 1440;
                y = 0;
                break;
            default:
                break;
        }

    }
}


[System.Serializable]
public class AdbCmdInstallApkParameter : AdbCmdParameterAbstract
{


    public override string GetTheCommand(string targetDevicesID)
    {

        string cmd = "adb";
        string appPath="";
        if (File.Exists(m_apkPath))
            appPath = m_apkPath;
        else if (File.Exists(AdbUtillity.m_apkFolderPath + "\\" + m_apkPath))
            appPath = AdbUtillity.m_apkFolderPath + "\\" + m_apkPath;
        else if (File.Exists(Application.dataPath + "\\" + m_apkPath))
            appPath = Application.dataPath + "\\" + m_apkPath;

        if (!string.IsNullOrWhiteSpace(targetDevicesID))
            cmd += " -s " + targetDevicesID;
        cmd += " install -r " + appPath;

        return cmd;
    }
    public string m_apkPath;
    public void SetApkPath(string value)
    {
       
        m_apkPath =StringUtility.FinishBy(value, ".apk");

    }
}
[System.Serializable]
public class AdbCmdUninstallApkParameter : AdbCmdParameterAbstract
{


    public override string GetTheCommand(string targetDevicesID)
    {

        string cmd = "adb";

        if (!string.IsNullOrWhiteSpace(targetDevicesID))
            cmd += " -s " + targetDevicesID;
        cmd += " uninstall " + m_appNameSpace;

        return cmd;
    }
    public string m_appNameSpace;

    public void SetNamespace(string value)
    {
        m_appNameSpace = value;
    }

}
[System.Serializable]
public class AdbCmdLaunchApkParameter : AdbCmdParameterAbstract
{


    public override string GetTheCommand(string targetDevicesID)
    {
        string cmd = "adb";

        if (!string.IsNullOrWhiteSpace(targetDevicesID))
            cmd += " -s " + targetDevicesID;
        cmd += string.Format(m_cmdFormat, m_appNamespace);

        return cmd;
    }
    public string m_appNamespace;
    public string m_cmdFormat = " shell monkey -p {0} -c android.intent.category.LAUNCHER 1";
    public void SetNamespace(string value)
    {
        m_appNamespace = value;
    }

}
[System.Serializable]
public class AdbCmdExtractApkParameter : AdbCmdParameterAbstract
{


    public override string GetTheCommand(string targetDevicesID)
    {
        string cmd = "adb";

        if (!string.IsNullOrWhiteSpace(targetDevicesID))
            cmd += " -s " + targetDevicesID;
        cmd += string.Format(m_cmdFormat, m_applicationPath, m_destinationFolder, m_applicationName);

        return cmd;
    }
    public string m_applicationPath;
    public string m_destinationFolder;
    public string m_applicationName;
    public string m_cmdFormat = " pull {0} {1}/{2}";
    public void SetApkPathOnPhone(string value)
    {
        value = StringUtility.FinishBy(value, ".apk");
        m_applicationPath = value;
    }
    public void SetDestinationFolder(string value)
    {
        m_destinationFolder = value;
    }
    public void SetApplicationName(string value)
    {
        value= StringUtility.FinishBy(value,".apk");
        m_applicationName = value;
    }

    public string GetWindowPath()
    {
        return m_destinationFolder + "/" + m_applicationName;
    }
}


public class StringUtility
{
    public static bool IsFinishingBy(string text, string endWith)
    {
        int index = text.ToLower().LastIndexOf(endWith);
        if (index < 0)
            return false;

        if (index + endWith.Length == text.Length)
            return true;
        return false;
    }

    public static string FinishBy(string value, string endWith)
    {
        if (IsFinishingBy(value, endWith))
            return value;
        return value + endWith;
    }
}


public class ProcessKillTime {

    public ProcessKillTime(Process process, Thread linkedThread, int maxLivingTimeMiliseconds) {
        m_deathTime = DateTime.Now.AddMilliseconds(maxLivingTimeMiliseconds);
        m_process = process;
        m_linkedThread = linkedThread;
    }

    public Process m_process;
    public Thread m_linkedThread;
    public DateTime m_deathTime;

    public bool ReadyToBeKilled() {
        return m_deathTime < DateTime.Now;
    }

    public void Kill()
    {
        m_process.Close();
        m_process.Kill();
        m_linkedThread.Abort();
    }


}

[System.Serializable]
public class AndroidDevice {

    public string m_deviceId = "";
    public string m_aliasName="";
    public string m_product="";
    public DeviceState m_state = DeviceState.Offline;
    public DeviceType m_deviceModel = DeviceType.Unkown;
    public string m_deviceName ="";
    public string m_transportId = "";

    public enum DeviceState {
        Device, Unauthorized, Offline
    }

    public enum DeviceType {Unkown, OculusQuest, OculusGo, GearVR_Note4}
}
