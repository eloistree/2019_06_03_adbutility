﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

public class AdbUtilityMono : MonoBehaviour
{
    public string m_adbFolderPath = "";
    public string m_scrcopyFolderPath = "";
    public string m_applicationFolder = "";

    public void ExtractPackage(AdbCmdExtractApkParameter extractPackage)
    {
        AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
        AdbUtillity.ApplyAdbCmdParameter(extractPackage, m_devicesId);
    }

  

    public string m_screenshotFolderPath = "";
    public  string m_appDataFolderPath= "";



    public ConsoleOutputEvent m_onConsoleOutput;
    public ConsoleOutputEvent m_onConsoleError;

    public string[] m_devicesId;

   

    public string m_lastOutput;

    public int m_maxOutputTime = 5000;

    public void Start()
    {
        AdbUtillity.m_maxOutputRecord = m_maxOutputTime;
    }




    public void ApplyCommands( string[] commands)
    {
        for (int i = 0; i < commands.Length; i++)
        {
            ApplyCommands(commands[i]);
        }
    }

  
    public void ApplyCommands(float timeBetweenCommands, string[] commands)
    {
        if (timeBetweenCommands <= 0)
            ApplyCommands(commands);
        else 
            StartCoroutine(ApplyCommandsCoroutine(timeBetweenCommands, commands));
    }

    private IEnumerator ApplyCommandsCoroutine(float timeBetweenCommands, string[] commands)
    {

        for (int i = 0; i < commands.Length; i++)
        {
            ApplyCommands(commands[i]);
            yield return new WaitForSeconds(timeBetweenCommands);
        }
        yield break;
    }

    public void LaunchApplication(AdbCmdLaunchApkParameter parameters)
    {
        AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
        AdbUtillity.ApplyAdbCmdParameter(parameters,m_devicesId);
    }

    public void KillAllApplications() {

        AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
        RawOuputCallBack callback;
        AdbUtillity.ApplyCommandsWithoutReturn(m_adbFolderPath, "adb shell am kill-all");

    }



    public void SetAdbFolderPath(string path)
    {
        m_adbFolderPath = AdbUtillity.m_adbFolderPath = path;
    }
    public void SetApplicationFolderPath(string path)
    {
        m_applicationFolder = AdbUtillity.m_apkFolderPath = path;
    }
    public void SetScreenRecorderFolderPath(string path)
    {

        m_scrcopyFolderPath = AdbUtillity.m_scrcopyFolderPath = path;
    }
    public void SetScreenshotFolderPath(string path)
    {

        m_screenshotFolderPath = AdbUtillity.m_screenshotFolderPath = path;
    }
    public void SetAppDataFolderPath(string path)
    {

        m_appDataFolderPath = AdbUtillity.m_appDataFolderPath = path;
    }


    public void DisplayIpAddress()
    {
        RawOuputCallBack callback;
        AdbUtillity.SetAdbFolderPath(m_adbFolderPath);

        AdbUtillity.DisplayIpAddress(m_devicesId);
        
    }
    public void DisplayAdbDevices()
    {
            RawOuputCallBack callback;
            AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
            AdbUtillity.GetDeviceList(out callback);

    }

    public void DisplayApplicationInstalled()
    {
        for (int i = 0; i < m_devicesId.Length; i++)
        {
            RawOuputCallBack callback;
            AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
            AdbUtillity.GetApplicationInstallation(out callback, m_devicesId[i], m_maxOutputTime);

        }
    }

    public void OpenWindowConsole() {
        AdbUtillity.OpenWindowConsole();
    }

    public void StartDefaultScreenCasting()
    {

        StartDefaultScreenCasting(AdbCmdScreencastParameter.CreateDefault());
    }

    public void StartDefaultScreenCasting(AdbCmdScreencastParameter parameter)
    {

        AdbUtillity.SetScreenRecorderFolderPath(m_scrcopyFolderPath);
        StartCoroutine(StartDefaultScreenCastingForDevices(parameter, m_devicesId));

    }

    public IEnumerator StartDefaultScreenCastingForDevices(AdbCmdScreencastParameter parameter, string [] devicesId)
    {

        AdbUtillity.SetScreenRecorderFolderPath(m_scrcopyFolderPath);
        for (int i = 0; i < m_devicesId.Length; i++)
        {
            AdbUtillity.StartScreenCastingWithThread(parameter, m_devicesId[i], m_devicesId.Length <= 1);
            yield return new WaitForSeconds(3);
        }
    }
    public void Install(string apkPath)
    {
        AdbCmdInstallApkParameter installparams = new AdbCmdInstallApkParameter();
        installparams.SetApkPath(apkPath);
        Install(installparams);
    }
        public void Install(AdbCmdInstallApkParameter parameters)
    {
        //File.Exists(parameters.m_apkPath)
        //if(parameters.)
        AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
        AdbUtillity.ApplyAdbCmdParameter(parameters, m_devicesId);
    }
    public void Uninstall(AdbCmdUninstallApkParameter parameters)
    {
        AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
        AdbUtillity.ApplyAdbCmdParameter(parameters, m_devicesId);
    }


    public void KillAdbServer() {
        
        AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
        AdbUtillity.KillAdbServer();
    }

    public void LaunchBluetoothSetting()
    {
        for (int i = 0; i < m_devicesId.Length; i++)
        {
        AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
        AdbUtillity.LaunchBluetoothSetting(m_devicesId[i]);

        }

    }
    public void RestartTheDevice()
    {
        for (int i = 0; i < m_devicesId.Length; i++)
        {
            AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
            AdbUtillity.RestartTheDevice(m_devicesId[i]);

        }

    }
    public void TurnDeviceOff()
    {
        for (int i = 0; i < m_devicesId.Length; i++)
        {
            AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
            AdbUtillity.TurnDeviceOff(m_devicesId[i]);

        }

    }
    public void StartTcpIpOnDevice(int port)
    {
        AdbUtillity.StartTcpIpOnDevice(port, m_devicesId);
    }
    public void ConnectToIpAddress(string text)
    {
        AdbUtillity.ConnectToIpAddress(text);
    }



    public void ApplyCommands( string  command  ) {


        if (m_devicesId.Length > 0)
        {
            for (int i = 0; i < m_devicesId.Length; i++)
            {
                string targetedCmd = AdbUtillity.SetCommandForThisTarget(m_devicesId[i], command);
                AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
                RawOuputCallBack b;
                AdbUtillity.ApplyCommands(m_adbFolderPath,200,out b, targetedCmd);
                //AdbUtillity.ApplyCommandsWithoutReturn(m_adbFolderPath, targetedCmd);
            }
        }
        else
        {
            AdbUtillity.SetAdbFolderPath(m_adbFolderPath);
            AdbUtillity.ApplyCommandsWithoutReturn(m_adbFolderPath, command);

        }
    }

    public void OpenNotePad()
    {
        AdbUtillity.OpenNotePad("Hey mon ami");
    }


    public void OpenNotePad(string text)
    {
        AdbUtillity.OpenNotePad(text);
    }

    private void Update()
    {
        while (AdbUtillity.m_output.Count > 0)
        {
            string data = AdbUtillity.m_output.Dequeue();
            ReceivedOutput(data);
        }
        while (AdbUtillity.m_outputError.Count > 0)
        {
            string data = AdbUtillity.m_outputError.Dequeue();
            ReceivedOutputError(data);
        }
    }


    public void Reset()
    {
        m_adbFolderPath = "C:\\adb\\adb";
    }




    private void ReceivedOutput(string value)
    {
        m_onConsoleOutput.Invoke(value);
    }

    private void ReceivedOutputError(string value)
    {
        m_onConsoleError.Invoke(value);
    }


   

}

[System.Serializable]
public class ConsoleOutputEvent : UnityEvent<string> { }
[System.Serializable]
public class ConsoleErrorEvent : UnityEvent<string> { }
public delegate void ConsoleOutputDelegate(string value);
public delegate void ConsoleErrorDelegate(string value);
public delegate void ConsoleOutputsDelegate(string [] value);
public delegate void ConsoleErrorsDelegate(string [] value);