﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_AdbUtility_SendAdbCommand : MonoBehaviour
{

    public AdbUtilityMono m_utility;
    
    public InputField m_cmd;

    public void Apply()
    {
        //RefreshParams();
        m_utility.ApplyCommands(m_cmd.text);
    }



    //public void OnValidate()
    //{
    //    RefreshParams();
    //}

    //private void RefreshParams()
    //{
    //    m_parameters.SetNamespace(m_namespace.text);
    //}
}