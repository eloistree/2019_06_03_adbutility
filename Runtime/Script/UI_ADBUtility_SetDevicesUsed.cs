﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ADBUtility_SetDevicesUsed : MonoBehaviour
{


    public AdbUtilityMono m_adbUtility;
    public InputField m_devicesToUse;

    public GameObject m_prefab;
    public RectTransform m_whereToAdd;
    [Header("Debug")]
    public string[] m_lines;
    public List<DeviceAvailable> m_devices;
    public List<UI_ADBUtility_DeviceToUse> m_uiDevices;

    [System.Serializable]
    public class DeviceAvailable
    {
        public bool m_usedIt;
        public AndroidDevice m_device;
    }


    void Start()
    {
        Refresh();
    }

    private void Refresh(string value)
    {
        Refresh();
    }


    public void Refresh()
    {
        AdbUtillity.ClearRegisteredDevices();
        string fullText = RemoveAllMultiSpace(m_devicesToUse.text);
        m_lines = fullText.Split('\r');

        //192.168.1.114:5555 device product:vr_monterey model:Quest device:monterey transport_id:8

        for (int i = 0; i < m_lines.Length; i++)
        {
            AndroidDevice d = new AndroidDevice();
            string[] tokens = m_lines[i].Split(' ');
            if (tokens.Length > 0)
                d.m_deviceId = tokens[0];
            for (int j = 1; j < tokens.Length; j++)
            {
                string token = tokens[j];
                if (token.IndexOf("product:") > -1)
                    d.m_product = token.Replace("product:", "");
                else if (token.ToLower() == ("device"))
                    d.m_state = AndroidDevice.DeviceState.Device;
                else if (token.ToLower() == ("offline"))
                    d.m_state = AndroidDevice.DeviceState.Offline;
                else if (token.ToLower() == ("unauthorized"))
                    d.m_state = AndroidDevice.DeviceState.Unauthorized;
                else if (token.IndexOf("model:") > -1)
                {
                    string value = token.Replace("model:", "");
                    if (value == "Quest")
                        d.m_deviceModel = AndroidDevice.DeviceType.OculusQuest;
                    else if (value == "Pacific")
                        d.m_deviceModel = AndroidDevice.DeviceType.OculusGo;
                    else if (value == "SM_N910F")
                        d.m_deviceModel = AndroidDevice.DeviceType.GearVR_Note4;
                    

                }
                else if (token.IndexOf("device:") > -1)
                {
                    string value = token.Replace("device:", "");
                    d.m_deviceName = value;

                }
                else if (token.IndexOf("transport_id:") > -1)
                {
                    string value = token.Replace("transport_id:", "");
                    d.m_transportId = value;

                }
                else if (token.IndexOf("alias:") > -1)
                {
                    string value = token.Replace("alias:", "");
                    d.m_aliasName = value;

                }
            }
            if(!string.IsNullOrWhiteSpace(d.m_deviceId) && !string.IsNullOrWhiteSpace(d.m_state.ToString()))
                AdbUtillity.AddRegisteredDevices(d);
            
        }

        m_devices.Clear();
        m_uiDevices.Clear();
        Clear(m_whereToAdd);
        List<AndroidDevice> devices = AdbUtillity.GetRegisteredDevice();
        foreach (AndroidDevice device in devices)
        {
            DeviceAvailable availaibleDevice = new DeviceAvailable() { m_usedIt = true, m_device = device };
            m_devices.Add(availaibleDevice);

            UI_ADBUtility_DeviceToUse ui = CreateUIFor(availaibleDevice);
            m_uiDevices.Add(ui);

        }
        RefreshGroupState(false);


    }
    public void Clear( Transform transform)
    {
        for (int i = transform.childCount-1; i >=0 ; i--)
        {

            Destroy(transform.GetChild(i).gameObject);

        }
    }


    public void SelectAll(bool on)
    {

        foreach (var item in m_uiDevices)
        {
            item.m_isUsingDevice.isOn = on;

        }

    }
    private UI_ADBUtility_DeviceToUse CreateUIFor(DeviceAvailable d)
    {
        GameObject created = GameObject.Instantiate(m_prefab);
        RectTransform rect = created.GetComponent<RectTransform>();
        rect.SetParent(m_whereToAdd);
        UI_ADBUtility_DeviceToUse ui = created.GetComponent<UI_ADBUtility_DeviceToUse>();
        ui.SetAlias(d.m_device.m_aliasName);
        ui.SetId(d.m_device.m_deviceId);
        ui.SetModel(d.m_device.m_deviceModel.ToString());
        ui.SetUseState(false);
        ui.ListenToOnChange(RefreshGroupState);

        created.SetActive(true);
        return ui;
    }

    List<string> m_targetId = new List<string>();
    private void RefreshGroupState(bool arg0)
    {
        m_targetId.Clear();
        for (int i = 0; i < m_uiDevices.Count; i++)
        {
            if (m_uiDevices[i].m_isUsingDevice.isOn) {
                m_targetId.Add(m_uiDevices[i].m_deviceId.text);
            }
        }
        m_adbUtility.m_devicesId = m_targetId.ToArray();
    }

    public string RemoveAllMultiSpace(string text ) {
        while (text.IndexOf("  ") > -1) {
            text = text.Replace("  ", " ");
        }
        return text;
    }
}
