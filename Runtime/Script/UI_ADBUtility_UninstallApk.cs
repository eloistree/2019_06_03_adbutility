﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ADBUtility_UninstallApk : MonoBehaviour
{

    public AdbUtilityMono m_utility;
    public AdbCmdUninstallApkParameter m_parameters;

    public InputField m_namespace;

    public void Apply()
    {
        RefreshParams();
        m_utility.Uninstall(m_parameters);
    }



    public void OnValidate()
    {
        RefreshParams();
    }

    private void RefreshParams()
    {
        m_parameters.SetNamespace(m_namespace.text);
    }
}