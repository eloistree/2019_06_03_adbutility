﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_ADBUtility_DeviceToUse : MonoBehaviour
{

    public Toggle m_isUsingDevice;
    public Text m_deviceAlias;
    public Text m_deviceId;
    public Text m_deviceModel;
    public Image m_background;
    
    




    void Start()
    {
        m_isUsingDevice.onValueChanged.AddListener(Refresh);
        Refresh();
    }

  
    public void SetUseState(bool isOn) { m_isUsingDevice.isOn = isOn; Refresh(); }
    public void SetAlias(string value) { m_deviceAlias.text = value; }
    public void SetId(string value) { m_deviceId.text = value; }
    public void SetModel(string value) { m_deviceModel.text = value; }

    public void ListenToOnChange(UnityAction<bool> action) {

        m_isUsingDevice.onValueChanged.AddListener(action);
    }

    private void ChangeColor(bool value)
    {
        m_background.color = m_isUsingDevice.isOn ? Color.green : Color.red;
    
    }

    private void Refresh()
    {
        ChangeColor(m_isUsingDevice.isOn);
    }
    private void Refresh(bool value)
    {
        Refresh();
    }

}
