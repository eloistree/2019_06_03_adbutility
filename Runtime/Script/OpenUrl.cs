﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenUrl : MonoBehaviour
{
    public string m_url;
    public void Open()
    {
        Open(m_url);
    }
        public void Open(string url)
        {
            Application.OpenURL(url);
    }
    public void SetUrl(string url) {
        m_url = url;
    }
}
