﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPanelMove : MonoBehaviour
{
    public RectTransform m_mainPanel;

    public float m_pourcentValue;
    public Vector2 targetPos;

    public void MovePanel(float pourcent) {


    }

    public void MoveLeft(float pourcent){

        targetPos.x += m_mainPanel.rect.width * pourcent;
    }
    public void MoveRigth(float pourcent)
    {
        MoveLeft(-pourcent);
    }

    public void Update()
    {
        m_mainPanel.offsetMin = Vector2.Lerp(m_mainPanel.offsetMin, targetPos,  Time.deltaTime*3);
        m_mainPanel.offsetMax = Vector2.Lerp(m_mainPanel.offsetMax, targetPos, Time.deltaTime*3);
    }

   
}
