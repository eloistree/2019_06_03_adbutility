﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdbUtilityConsoleDebug : MonoBehaviour
{
    public AdbUtilityMono m_utilityMono;
    public ConsoleOutputEvent m_onConsoleOutput;
    public ConsoleOutputEvent m_onConsoleError;
    public bool m_useDebugLog;
    
    private void OnEnable()
    {
        m_utilityMono.m_onConsoleOutput.AddListener(ReceivedOutput);
        m_utilityMono.m_onConsoleError.AddListener(ReceivedOutput);

    }
    private void OnDisable()
    {
        m_utilityMono.m_onConsoleOutput.RemoveListener(ReceivedOutput);
        m_utilityMono.m_onConsoleError.RemoveListener(ReceivedOutput);

    }


    private void ReceivedOutput(string value)
    {
        if (m_useDebugLog)
            Debug.Log("Output: " + value);
        m_onConsoleOutput.Invoke(value);
    }

    private void ReceivedOutputError(string value)
    {
        if (m_useDebugLog)
            Debug.Log("Error: " + value);
        m_onConsoleError.Invoke(value);
    }

    

}
