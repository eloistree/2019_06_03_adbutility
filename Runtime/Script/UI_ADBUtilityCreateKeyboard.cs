﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ADBUtilityCreateKeyboard : MonoBehaviour
{
    public GameObject m_prefab;
    public ButtonValue[] m_buttons;
    public RectTransform m_whereToAdd;

    [System.Serializable]
    public class ButtonValue {
        public string m_name;
        public string m_keycodeName;
    }
    

    void Start()
    {
        for (int i = 0; i < m_buttons.Length; i++)
        {
            GameObject created = GameObject.Instantiate(m_prefab);
            RectTransform rect = created.GetComponent<RectTransform>();
            AdbUtility_SendAdbCommand send = created.GetComponent<AdbUtility_SendAdbCommand>();
            send.m_commands = new string[] {"adb shell input keyevent "+ m_buttons[i].m_keycodeName };
            Text t = created.GetComponentInChildren<Text>();
            t.text = m_buttons[i].m_name;

            rect.SetParent(m_whereToAdd);
            created.SetActive(true);
        }
        
    }


    private void Reset()
    {
        m_buttons = new ButtonValue[] {
            new ButtonValue(){m_name ="Soft Left" , m_keycodeName= "KEYCODE_SOFT_LEFT" },
            new ButtonValue(){m_name ="Soft Right" , m_keycodeName= "KEYCODE_SOFT_RIGHT" },
            new ButtonValue(){m_name ="Home" , m_keycodeName= "KEYCODE_HOME" },
            new ButtonValue(){m_name ="Back" , m_keycodeName= "KEYCODE_BACK" },
            new ButtonValue(){m_name ="Call" , m_keycodeName= "KEYCODE_CALL" },
            new ButtonValue(){m_name ="End Call" , m_keycodeName= "KEYCODE_ENDCALL" },
            new ButtonValue(){m_name ="0" , m_keycodeName= "KEYCODE_0" },
            new ButtonValue(){m_name ="1" , m_keycodeName= "KEYCODE_1" },
            new ButtonValue(){m_name ="2" , m_keycodeName= "KEYCODE_2" },
            new ButtonValue(){m_name ="3" , m_keycodeName= "KEYCODE_3" },
            new ButtonValue(){m_name ="4" , m_keycodeName= "KEYCODE_4" },
            new ButtonValue(){m_name ="5" , m_keycodeName= "KEYCODE_5" },
            new ButtonValue(){m_name ="6" , m_keycodeName= "KEYCODE_6" },
            new ButtonValue(){m_name ="7" , m_keycodeName= "KEYCODE_7" },
            new ButtonValue(){m_name ="8" , m_keycodeName= "KEYCODE_8" },
            new ButtonValue(){m_name ="9" , m_keycodeName= "KEYCODE_9" },
            new ButtonValue(){m_name ="*" , m_keycodeName= "KEYCODE_STAR" },
            new ButtonValue(){m_name ="£" , m_keycodeName= "KEYCODE_POUND" },
            new ButtonValue(){m_name ="Up" , m_keycodeName= "KEYCODE_DPAD_UP" },
            new ButtonValue(){m_name ="Down" , m_keycodeName= "KEYCODE_DPAD_DOWN" },
            new ButtonValue(){m_name ="Left" , m_keycodeName= "KEYCODE_DPAD_LEFT" },
            new ButtonValue(){m_name ="Right" , m_keycodeName= "KEYCODE_DPAD_RIGHT" },
            new ButtonValue(){m_name ="Center" , m_keycodeName= "KEYCODE_DPAD_CENTER" },
            new ButtonValue(){m_name ="Vol +" , m_keycodeName= "KEYCODE_VOLUME_UP" },
            new ButtonValue(){m_name ="Vol -" , m_keycodeName= "KEYCODE_VOLUME_DOWN" },
            new ButtonValue(){m_name ="Power" , m_keycodeName= "KEYCODE_POWER" },
            new ButtonValue(){m_name ="Camera" , m_keycodeName= "KEYCODE_CAMERA" },
            new ButtonValue(){m_name ="Clear" , m_keycodeName= "KEYCODE_CLEAR" },
            new ButtonValue(){m_name ="a" , m_keycodeName= "KEYCODE_A" },
            new ButtonValue(){m_name ="b" , m_keycodeName= "KEYCODE_B" },
            new ButtonValue(){m_name ="c" , m_keycodeName= "KEYCODE_C" },
            new ButtonValue(){m_name ="d" , m_keycodeName= "KEYCODE_D" },
            new ButtonValue(){m_name ="e" , m_keycodeName= "KEYCODE_E" },
            new ButtonValue(){m_name ="f" , m_keycodeName= "KEYCODE_F" },
            new ButtonValue(){m_name ="g" , m_keycodeName= "KEYCODE_G" },
            new ButtonValue(){m_name ="h" , m_keycodeName= "KEYCODE_H" },
            new ButtonValue(){m_name ="i" , m_keycodeName= "KEYCODE_I" },
            new ButtonValue(){m_name ="j" , m_keycodeName= "KEYCODE_J" },
            new ButtonValue(){m_name ="k" , m_keycodeName= "KEYCODE_K" },
            new ButtonValue(){m_name ="l" , m_keycodeName= "KEYCODE_L" },
            new ButtonValue(){m_name ="m" , m_keycodeName= "KEYCODE_M" },
            new ButtonValue(){m_name ="n" , m_keycodeName= "KEYCODE_N" },
            new ButtonValue(){m_name ="o" , m_keycodeName= "KEYCODE_O" },
            new ButtonValue(){m_name ="p" , m_keycodeName= "KEYCODE_P" },
            new ButtonValue(){m_name ="q" , m_keycodeName= "KEYCODE_Q" },
            new ButtonValue(){m_name ="r" , m_keycodeName= "KEYCODE_R" },
            new ButtonValue(){m_name ="s" , m_keycodeName= "KEYCODE_S" },
            new ButtonValue(){m_name ="t" , m_keycodeName= "KEYCODE_T" },
            new ButtonValue(){m_name ="u" , m_keycodeName= "KEYCODE_U" },
            new ButtonValue(){m_name ="v" , m_keycodeName= "KEYCODE_V" },
            new ButtonValue(){m_name ="w" , m_keycodeName= "KEYCODE_W" },
            new ButtonValue(){m_name ="x" , m_keycodeName= "KEYCODE_X" },
            new ButtonValue(){m_name ="y" , m_keycodeName= "KEYCODE_Y" },
            new ButtonValue(){m_name ="z" , m_keycodeName= "KEYCODE_Z" },
            new ButtonValue(){m_name ="," , m_keycodeName= "KEYCODE_COMMA" },
            new ButtonValue(){m_name ="." , m_keycodeName= "KEYCODE_PERIOD" },
            new ButtonValue(){m_name ="Alt Left" , m_keycodeName= "KEYCODE_ALT_LEFT" },
            new ButtonValue(){m_name ="Alt Right" , m_keycodeName= "KEYCODE_ALT_RIGHT" },
            new ButtonValue(){m_name ="Shift Left" , m_keycodeName= "KEYCODE_SHIFT_LEFT" },
            new ButtonValue(){m_name ="Shift Rigth" , m_keycodeName= "KEYCODE_SHIFT_RIGHT" },
            new ButtonValue(){m_name ="Tab" , m_keycodeName= "KEYCODE_TAB" },
            new ButtonValue(){m_name ="Space" , m_keycodeName= "KEYCODE_SPACE" },
            new ButtonValue(){m_name ="Sym ?" , m_keycodeName= "KEYCODE_SYM" },
            new ButtonValue(){m_name ="Explorer" , m_keycodeName= "KEYCODE_EXPLORER" },
            new ButtonValue(){m_name ="Envelope" , m_keycodeName= "KEYCODE_ENVELOPE" },
            new ButtonValue(){m_name ="Enter" , m_keycodeName= "KEYCODE_ENTER" },
            new ButtonValue(){m_name ="Delete" , m_keycodeName= "KEYCODE_DEL" },
            new ButtonValue(){m_name ="`" , m_keycodeName= "KEYCODE_GRAVE" },
            new ButtonValue(){m_name ="-" , m_keycodeName= "KEYCODE_MINUS" },
            new ButtonValue(){m_name ="=" , m_keycodeName= "KEYCODE_EQUALS" },
            new ButtonValue(){m_name ="[" , m_keycodeName= "KEYCODE_LEFT_BRACKET" },
            new ButtonValue(){m_name ="]" , m_keycodeName= "KEYCODE_RIGHT_BRACKET" },
            new ButtonValue(){m_name ="\\" , m_keycodeName= "KEYCODE_BACKSLASH" },
            new ButtonValue(){m_name =";" , m_keycodeName= "KEYCODE_SEMICOLON" },
            new ButtonValue(){m_name ="'" , m_keycodeName= "KEYCODE_APOSTROPHE" },
            new ButtonValue(){m_name ="/" , m_keycodeName= "KEYCODE_SLASH" },
            new ButtonValue(){m_name ="@" , m_keycodeName= "KEYCODE_AT" },
            new ButtonValue(){m_name ="Num" , m_keycodeName= "KEYCODE_NUM" },
            new ButtonValue(){m_name ="Headsethook" , m_keycodeName= "KEYCODE_HEADSETHOOK" },
            new ButtonValue(){m_name ="Cam Focus" , m_keycodeName= "KEYCODE_FOCUS" },
            new ButtonValue(){m_name ="+" , m_keycodeName= "KEYCODE_PLUS" },
            new ButtonValue(){m_name ="Menu" , m_keycodeName= "KEYCODE_MENU" },
            new ButtonValue(){m_name ="Notification" , m_keycodeName= "KEYCODE_NOTIFICATION" },
            new ButtonValue(){m_name ="Search" , m_keycodeName= "KEYCODE_SEARCH" },
            new ButtonValue(){m_name ="Pause" , m_keycodeName= "KEYCODE_MEDIA_PLAY_PAUSE" },
            new ButtonValue(){m_name ="Stop" , m_keycodeName= "KEYCODE_MEDIA_STOP" },
            new ButtonValue(){m_name ="Next" , m_keycodeName= "KEYCODE_MEDIA_NEXT" },
            new ButtonValue(){m_name ="Previous" , m_keycodeName= "KEYCODE_MEDIA_PREVIOUS" },
            new ButtonValue(){m_name ="Rewind" , m_keycodeName= "KEYCODE_MEDIA_REWIND" },
            new ButtonValue(){m_name ="Fast Forward" , m_keycodeName= "KEYCODE_MEDIA_FAST_FORWARD" },
            new ButtonValue(){m_name ="Mute" , m_keycodeName= "KEYCODE_MUTE" },
            new ButtonValue(){m_name ="Page up" , m_keycodeName= "KEYCODE_PAGE_UP" },
            new ButtonValue(){m_name ="Page Down" , m_keycodeName= "KEYCODE_PAGE_DOWN" },
            new ButtonValue(){m_name ="Go to Symbols" , m_keycodeName= "KEYCODE_PICTSYMBOLS" },
            new ButtonValue(){m_name ="Move Home?" , m_keycodeName= "KEYCODE_MOVE_HOME" },
            new ButtonValue(){m_name ="Move End ?" , m_keycodeName= "KEYCODE_MOVE_END" }

        };
        //MORE HERE: https://developer.android.com/reference/android/view/KeyEvent


    }
}
