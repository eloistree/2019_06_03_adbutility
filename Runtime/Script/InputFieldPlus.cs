﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldPlus : MonoBehaviour
{
    public InputField m_textLinked;


    public void AppendAtStart(string value)
    {

        Append(value, true);
    }
    public void AppendAtEnd(string value)
    {
        Append(value, false);
        MoveCursorDown();

    }

    public void Append(string value, bool atStart)
    {
        Append(new string[] { value }, atStart);
    }
    public void Append( string[] value, bool atStart)
    {
        for (int i = 0; i < value.Length; i++)
        {
            if (atStart)
                m_textLinked.text = value[i]+"\n\r" +
            m_textLinked.text;
            else
                m_textLinked.text = 
            m_textLinked.text + "\n\r" + value[i];

        }
    }


    public void  MoveCursorDown() {

        m_textLinked.MoveTextEnd(false);    
        m_textLinked.ForceLabelUpdate();
    }

    public void Reset()
    {
        if(m_textLinked==null)
        m_textLinked = GetComponent<InputField>();
    }

}
