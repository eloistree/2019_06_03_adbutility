﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;


public class CopyPast_AdbUtility : MonoBehaviour
{
    public AdbUtilityMono m_adbUtility;

    public string copyPastValue;
 
    void Update()
    {
        copyPastValue = ClipboardHelper.clipBoard;
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.V)) {

            if (StringUtility.IsFinishingBy(copyPastValue, ".apk")) {

                AdbCmdInstallApkParameter p = new AdbCmdInstallApkParameter();
                p.SetApkPath(copyPastValue);
                m_adbUtility.Install(p);
            }
        }
    }
}

 public class ClipboardHelper
{
   
    public static string clipBoard
    {
        get { return GUIUtility.systemCopyBuffer; }
        set { GUIUtility.systemCopyBuffer = value; }
    }
}