﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class UI_ADBUtility_Application : MonoBehaviour
{

    public AdbUtilityMono m_monoUtility;
    public string m_appName;
    public string m_apkFileName;
    public string m_apkNameSpace;
    public string m_downloadAPK;
    public Image m_loading;
    public Text m_appNameLabel;

   

    public void Install()
    {
        m_monoUtility.Install(new AdbCmdInstallApkParameter() { m_apkPath = m_apkFileName });
    }
    public void Uninstall()
    {
        m_monoUtility.Uninstall(new AdbCmdUninstallApkParameter() { m_appNameSpace = m_apkNameSpace });

    }
    public void Launch()
    {
        m_monoUtility.LaunchApplication(new AdbCmdLaunchApkParameter() { m_appNamespace = m_apkNameSpace });
    }
    public void DownloadTheApk()
    {
        if (string.IsNullOrEmpty(m_downloadAPK)) {
            return;
        }
        WWW www = new WWW(m_downloadAPK);
        StartCoroutine(ShowProgress(www));
        StartCoroutine(DownloadTheAPKCoroutine(www));

    }

    private IEnumerator ShowProgress(WWW www)
    {
        while (!www.isDone)
        {
            m_loading.fillAmount = 1f-www.progress;
            yield return new WaitForSeconds(.1f);
        }

        m_loading.fillAmount = 0f;
    }


    private IEnumerator DownloadTheAPKCoroutine(WWW www)
    {
        string appPath = GetPathOfTheAppInFolder();
        Debug.Log("Saved APK: " + appPath);
        yield return www;
        File.WriteAllBytes(appPath, www.bytes);

    }

    public string GetPathOfTheAppInFolder() {
        return AdbUtillity.GetAPKFolder() + "\\" + m_apkFileName;
    }

    private void OnValidate()
    {
        m_appNameLabel.text = m_appName;
    }
}
