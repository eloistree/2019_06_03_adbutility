﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdbUtility_SendAdbCommand : MonoBehaviour
{
    public AdbUtilityMono m_utility;
    public string[] m_commands = new string[] { "adb devices"};
    public float m_timeBetweenCommands=0;

    public void Apply()
    {
       
            m_utility.ApplyCommands(m_timeBetweenCommands,  m_commands);

    }

   
}
