﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ADBUtility_ToNotepad : MonoBehaviour
{
    public AdbUtilityMono m_adbUtility;
    public InputField m_inputField;

    public void Apply()
    {
        m_adbUtility.OpenNotePad(m_inputField.text);
        
    }
}
