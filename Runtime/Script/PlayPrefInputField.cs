﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayPrefInputField : MonoBehaviour
{

    public InputField m_linkedInput;
    public string m_id;
    public bool m_ifHaveContentOnly=true;



    public void Awake()
    {
        string saved = GetSavedInfo();
        if (m_ifHaveContentOnly && string.IsNullOrEmpty(saved))
        { }
        else m_linkedInput.text = saved;

        m_linkedInput.onValueChanged.AddListener(SaveInfo);
    }

    private string GetSavedInfo()
    {
        return PlayerPrefs.GetString(m_id);
    }

    private void SaveInfo(string value)
    {
        PlayerPrefs.SetString(m_id, value);
    }
    



    private void Reset()
    {
        m_id = "";
        for (int i = 0; i < 124; i++)
        {
            m_id += UnityEngine.Random.Range(0, 9);
        }
        m_linkedInput = GetComponent<InputField>();
    }
}
