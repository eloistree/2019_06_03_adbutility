﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ADBUtility_InstallApk : MonoBehaviour
{

    public AdbUtilityMono m_utility;
    public AdbCmdInstallApkParameter m_parameters;
    
    public InputField m_apkPath;

    public void Apply()
    {
        RefreshParams();
        m_utility.Install(m_parameters);
    }

 

    public void OnValidate()
    {
        RefreshParams();
    }

    private void RefreshParams()
    {
        m_parameters.SetApkPath(m_apkPath.text);
    }
}
