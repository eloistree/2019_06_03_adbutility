﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_AdbUtility_Screencast : MonoBehaviour
{
    public AdbUtilityMono m_utility;
    public AdbCmdScreencastParameter m_parameters;

    public InputField m_maxSize;
    public InputField m_bitRate;
    public Toggle m_fullScreen;
    public Toggle m_noDisplay;
    public Toggle m_touches;
    public Toggle m_onTop;

    public Toggle m_useCrop;
    public InputField m_cropWidth;
    public InputField m_cropHeight;
    public InputField m_cropX;
    public InputField m_cropY;


    public InputField m_recordPath;
    public Toggle m_addTime;
    private void RefreshParams()
    {



        m_parameters.SetRecordingTo(!string.IsNullOrEmpty(m_recordPath.text));

        if(m_addTime.isOn)
            m_parameters.SetRecordingPathTo(AddTimeToText(m_recordPath.text));
        else
            m_parameters.SetRecordingPathTo(m_recordPath.text);
        m_parameters.SetFullscreenTo(m_fullScreen.isOn);
        m_parameters.SetNotDisplayTo(m_noDisplay.isOn);
        m_parameters.SetTouchesTo(m_touches.isOn);
        m_parameters.SetCropTo(m_useCrop.isOn);
        m_parameters.SetCropValueTo(m_cropWidth.text, m_cropHeight.text, m_cropX.text, m_cropY.text);

        m_parameters.SetBitRateTo(!string.IsNullOrEmpty(m_bitRate.text));
        m_parameters.SetBitRateValueTo(m_bitRate.text);
        m_parameters.SetMaxSizeTo(!string.IsNullOrEmpty(m_maxSize.text));
        m_parameters.SetMaxSizeValueTo(m_maxSize.text);
        m_parameters.SetOnTopTo(m_onTop.isOn);

    }

    private string AddTimeToText(string text)
    {
        return text.ToLower().Replace(
            ".mp4", 
            DateTime.Now.ToString("yyyymmdd") + ".mp4");
    }

    public void Apply() {


        RefreshParams();

        m_utility.StartDefaultScreenCasting(m_parameters);
    }

    public void OnValidate()
    {
        RefreshParams();

    }


    public void ChangeSize (int sizeType){
        int width;
        int height;
        int x;
        int y;
        AdbCmdScreencastParameter.CropType type = AdbCmdScreencastParameter.CropType.QuestFullScreen;
        switch (sizeType)
        {
            case 0:
                type = AdbCmdScreencastParameter.CropType.QuestFullScreen;break;
            case 1:
                type = AdbCmdScreencastParameter.CropType.QuestLeftEye; break;
            case 2:
                type = AdbCmdScreencastParameter.CropType.QuestLeftRight; break;
            default:
                break;
        }

        AdbCmdScreencastParameter.GetValueFor(type, out width, out height, out x, out y);

        m_cropWidth.text = ""+width;
        m_cropHeight.text = "" + height;
        m_cropX.text = ""+x;
        m_cropY.text = ""+y;


    } 
 
}
