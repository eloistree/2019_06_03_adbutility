﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ADBUtility_TCPIP : MonoBehaviour
{

    public AdbUtilityMono m_utility;

    public InputField m_portField;

    public void Apply() {


        int port = 0;

        try {
            port = int.Parse(m_portField.text);
        }
        catch (Exception e) { };

        m_utility.StartTcpIpOnDevice(port);
    }

}
