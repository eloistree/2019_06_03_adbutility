﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_AdbUtility_SendInputText : MonoBehaviour
{
    public AdbUtilityMono m_utility;

    public InputField m_textToSend;

    public void Apply()
    {
        string text = m_textToSend.text;
        text = text.Replace(" ", "%s");
        m_utility.ApplyCommands("adb shell input text \""+text+"\"");
    }
    
}